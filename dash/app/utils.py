import base64
import io
from datetime import datetime, timedelta

import numpy as np
import pandas as pd
import plotly.express as px
import sqlalchemy as sql


starter_month = (11)
starter_month_str = "0"*abs(len(str(starter_month))-2) + str(starter_month)

MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep','Oct', 'Nov', 'Dec']

def connect():
    """connect to postgres database

    Returns:
        [type]: engine connexion
    """
    engine = sql.create_engine(
        "postgresql://postgres:mypassword@postgres:5432/postgres"
        # "postgresql://postgres:mypassword@192.168.10.176:5432/postgres"
    )
    return engine.connect()


def extracter_transformer(con):
    """get data from bilan table in postgres ddb

    Args:
        con ([type]): engine connexion

    Returns:
        pd.DataFrame: pandas dataframe of bilan
    """
    df = pd.read_sql("select * from bilan", con)

    # round
    col_round = df.columns.drop(['index', 'date'])
    df[col_round] = df[col_round].round()

    # Manage data
    df["day"] = df['date'].dt.strftime('%A')
    df["time"] = df.date.dt.strftime('%d')
    df["year"] = df.date.dt.strftime('%Y')
    df["month"] = df.date.dt.strftime('%m')
    df["month_str"] = df.date.dt.strftime('%b')
    df["week"] = df.date.dt.strftime('%W')

    df["date_same_year"] = df.date.apply(lambda x: x.replace(year=2020))

    return df


def manage_data(data):
    """preprocessing and formatting data

    Args:
        data (pd.DataFrame): bilan pandas dataframe

    Returns:
        pd.DataFrame: cleaned dataframe 
    """
    # drop first line and next dataset
    n = data['Unnamed: 0'].to_list().index(
        'CA ventilé Par Codes MC produits Simples (HT)')
    data = data.iloc[n:, :]
    data = data.drop(data.index[0])

    # data transposed
    datat = data.T
    datat.columns = datat.iloc[0, :]  #  changement des noms de colonnes

    datat = datat.drop([
        datat.index[len(datat)-1],
        datat.index[0],
    ])

    # date column
    datat['date'] = datat.index
    datat.date = pd.to_datetime(datat.date, dayfirst=True)
    datat.index = range(len(datat))
    datat.columns = [d.replace(' ', '_') for d in datat.columns]
    datat.columns = [d.replace('%', 'perc') for d in datat.columns]
    datat.columns = [d.lower() for d in datat.columns]

    # aggregate data
    annulation = ['debours', 'no_show']
    bar = ['bar_20perc', 'bar_10perc']
    se = ['soiree_etape', 'soiree_express']
    autres = datat.columns.drop(annulation + bar + se + ['total', 'date'])
    datat["autres"] = datat[autres].sum(axis=1)
    datat["annulation"] = datat[annulation].sum(axis=1)
    datat["bar"] = datat[bar].sum(axis=1)
    datat["se"] = datat[se].sum(axis=1)

    datat = datat[datat.columns.drop(
        np.concatenate((annulation, autres, bar, se)))]

    return datat


def push_to_db(data, con):
    """write data to database

    Args:
        data (pd.DataFrame): monthly data to push to bilan
        con ([type]): engine connexion
    """
    df = manage_data(data)
    try:
        df.to_sql("bilan", con, if_exists='append', method='multi')
    except ValueError as vx:
        print('push failed')
        print(vx)


def parse_contents(contents, filename):
    """decode csv file

    Args:
        contents ([type]): [description]
        filename (str): path

    Returns:
        [type]: [description]
    """
    content_type, content_string = contents.split(',')
    decoded = base64.b64decode(content_string)
    try:
        if 'csv' in filename:
            # Assume that the user uploaded a CSV file
            df = pd.read_csv(
                io.StringIO(decoded.decode('utf-8')))
            return df
        elif 'xls' in filename:
            # Assume that the user uploaded an excel file
            df = pd.read_excel(io.BytesIO(decoded))
            return df
    except Exception as e:
        print(e)
        return None

def reorder_months(starter_month):
    return [MONTHS[i] for i in range(starter_month - 1, 12)] + [MONTHS[i] for i in range(starter_month - 1)]

def create_table(df, mv):  # feed first table
    """create table

    Args:
        df (pd.DataFrame): pandas dataframe
        mv (list): starting month => june or not

    Returns:
        [type]: [description]
    """
    starter_month = mv[0] if len(mv) == 1 else mv[1]
    starter_month_str = "0"*abs(len(str(starter_month))-2) + str(starter_month)


    g = pd.read_json(df, orient='split')

    g.date_same_year = pd.to_datetime(g.date_same_year, unit='ms')

    g.year = g.year.astype(int)
    g.month = g.month.astype(int)

    g.year = np.where(g.date_same_year < pd.to_datetime(
        f'2020-{starter_month_str}-01'), g.year-1, g.year)
    g['month'] = g.month.apply(lambda x: np.where(x < starter_month, x+12, x))

    gmain = g[['year', 'month_str', 'month', 'total']].groupby(
        ['year', 'month_str', 'month']).sum().reset_index()

    gmain = gmain.drop('month', axis=1)
    gmain = gmain.pivot(index='year', columns='month_str', values='total')

    gmain['year'] = gmain.index
    gmain = gmain.reindex(columns=['year'] + reorder_months(starter_month))
    
    # iterate to find current month
    before_current = []
    for col in gmain.columns:
        if sum(gmain[gmain.year!=2015][col].isna()) > 0:
            current = col
            break
        if col != 'year':
            before_current.append(col)
    
    tot = pd.DataFrame(gmain[before_current].sum(axis=1), columns=['Auj'])
    tot['Total'] = gmain[gmain.columns.drop('year')].sum(axis=1)

    return [gmain, tot]


def init_feed_graph(selection, y_ref, ordo, slider, df, agg, data_type=None, mode=None, color=None):    # feed first graph
    """manage data and return

    Args:
        selection ([type]): [description]
        y_ref ([type]): [description]
        ordo ([type]): [description]
        slider ([type]): [description]
        df ([type]): [description]
        agg ([type]): [description]
        data_type ([type], optional): [description]. Defaults to None.
        mode ([type], optional): [description]. Defaults to None.
        color ([type], optional): [description]. Defaults to None.

    Returns:
        [type]: [description]
    """
    # couleur bleue "#325D88"
    df = pd.read_json(df, orient='split')
    selection.append(y_ref)
    selection = list(map(lambda x: int(x), selection))

    g = df[df.year.isin(selection)]
    yr = g.year.unique().tolist()
    if int(y_ref) in yr:
        yr.remove(int(y_ref))

    colorpaster = px.colors.qualitative.Pastel1

    g.date_same_year = pd.to_datetime(g.date_same_year, unit='ms')
    g = g[g.month >= slider[0]] if slider is not None else g
    g = g[g.month <= slider[1]] if slider is not None else g

    gmain = g.groupby(['year', agg]).sum().reset_index() if agg != "month_str" else g.groupby(
        ['year', "month", "month_str"]).sum().reset_index()

    # comp with ref
    dfcomp = gmain[gmain.year.isin(yr)].groupby(agg).mean().reset_index()
    t = [agg]
    [t.append(c + "_mean") for c in dfcomp.columns if c != agg]
    dfcomp.columns = t
    dfref = gmain[gmain.year == int(y_ref)]
    res = pd.merge(dfref, dfcomp, on=agg, how="left")
    res['diff'] = res[ordo] - res[f"{str(ordo)}_mean"]

    mi = min(res['diff'])
    ma = max(res['diff'])
    prop = 1 - ma/(ma - mi) if (ma - mi) > 0 else 1
    prop = max(prop, 0)
    if (color is None) & (agg != "month_str"):
        color = res['diff']
        colorscale = [[0, "#db4343"], [prop, "#ffffff"], [1, "#94db43"]]
        colorbar = dict(
            title="Différence"
        )
    elif (color is None) & (agg == "month_str"):
        color = res.sort_values("month")['diff']
        colorscale = [[0, "#db4343"], [prop, "#ffffff"], [1, "#94db43"]]
        colorbar = dict(
            title="Différence"
        )
    else:
        print(color)
        color = color
        colorscale = None
        colorbar = None

    data = [
        dict(
            type="bar" if data_type is None else data_type,
            mode=mode,
            x=gmain[agg][gmain.year == int(y_ref)] if agg != "month_str" else gmain[gmain.year == int(
                y_ref)].sort_values("month")[agg],
            y=gmain[ordo][gmain.year == int(y_ref)] if agg != "month_str" else gmain[gmain.year == int(
                y_ref)].sort_values("month")[ordo],
            marker=dict(color=color,
                        colorbar=colorbar,
                        colorscale=colorscale
                        ),
            hovertemplate="Chiffre :  %{y:,}<br>" +
            "Diff : %{marker.color:,}",
            name=str(y_ref)
        )
    ]
    for i, y in enumerate(yr):
        data.append(
            dict(
                type="scatter" if data_type is None else data_type,
                mode="markers" if mode is None else mode,
                x=gmain[agg][gmain.year == y] if agg != "month_str" else gmain[gmain.year == y].sort_values("month")[
                    agg],
                y=gmain[ordo][gmain.year == y] if agg != "month_str" else gmain[gmain.year == y].sort_values("month")[
                    ordo],
                marker=dict(color=colorpaster[i+1]),
                name=str(y),
                opacity=1
            )
        )
    layout = dict(
        autosize=True,
        automargin=True,
        margin=dict(l=30, r=30, b=20, t=40),
        hovermode="x",
        hovertemplate=None,
        plot_bgcolor="#F9F9F9",
        paper_bgcolor="#F9F9F9",
        # legend=dict(font=dict(size=10), orientation="h"),
        title=f"Evolution du chiffre d'affaire {ordo}",
        colorscale='Inferno'
    )
    return dict(data=data, layout=layout)


# graph cum sum
def feed_graph_cumsum(selection, y_ref, ordo, df, agg, mv, data_type=None, mode=None, color=None,):
    # couleur bleue "#325D88"
    starter_month = mv[0] if len(mv) == 1 else mv[1]
    starter_month_str = "0"*abs(len(str(starter_month))-2) + str(starter_month)

    MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep','Oct', 'Nov', 'Dec']
    df = pd.read_json(df, orient='split')
    selection.append(y_ref)
    selection = list(map(lambda x: int(x), selection))
    g = df[df.year.isin(selection)]
    yr = g.year.unique().tolist()
    if int(y_ref) in yr:
        yr.remove(int(y_ref))
    colorpaster = px.colors.qualitative.Pastel1

    g.date_same_year = pd.to_datetime(g.date_same_year, unit='ms')
    # g = g[g.month >= slider[0]] if slider is not None else g
    # g = g[g.month <= slider[1]] if slider is not None else g

    g.year = np.where(g.date_same_year < pd.to_datetime(
        f'2020-{starter_month_str}-01'), g.year-1, g.year)
    gmain = g.groupby(['year', agg]).sum().reset_index() if agg != "month_str" else g.groupby(
        ['year', "month", "month_str"]).sum().reset_index()

    # comp with ref
    dfcomp = gmain[gmain.year.isin(yr)].groupby(agg).mean().reset_index()
    t = [agg]
    [t.append(c + "_mean") for c in dfcomp.columns if c != agg]
    dfcomp.columns = t
    dfref = gmain[gmain.year == int(y_ref)]
    res = pd.merge(dfref, dfcomp, on=agg, how="left")
    res['diff'] = res[ordo] - res[str(ordo)+"_mean"]

    mi = min(res['diff'])
    ma = max(res['diff'])
    prop = 1 - ma/(ma - mi) if (ma - mi) > 0 else 1
    prop = max(prop, 0)
    if (color is None) & (agg != "month_str"):
        color = res['diff']
        colorscale = [[0, "#db4343"], [prop, "#ffffff"], [1, "#94db43"]]
        colorbar = dict(
            title="Différence"
        )
    elif (color is None) & (agg == "month_str"):
        color = res.sort_values("month")['diff']
        colorscale = [[0, "#db4343"], [prop, "#ffffff"], [1, "#94db43"]]
        colorbar = dict(
            title="Différence"
        )
    else:
        print(color)
        color = color
        colorscale = None
        colorbar = None

    # year begin in june zone
    # print(gmain.date_same_year)
    if agg == 'date_same_year':
        d = []
        for g in gmain.date_same_year:
            # print(g)
            if g < pd.to_datetime(f'2020-{starter_month_str}-01'):
                d.append(g)
            else:
                d.append(g.replace(year=2019))

        gmain['date_same_year'] = d
    elif agg == 'week':
        gmain['week'] = gmain.week.apply(lambda x: np.where(x < starter_month*4, x+52, x))
    else:
        gmain['month'] = gmain.month.apply(lambda x: np.where(x < starter_month, x+12, x))

    ##

    data = [
        dict(
            type="bar" if data_type is None else data_type,
            mode=mode,
            x=gmain[gmain.year == int(y_ref)].sort_values(agg)[agg] if agg != "month_str" else gmain[gmain.year == int(
                y_ref)].sort_values("month")[agg],
            y=gmain[gmain.year == int(y_ref)].sort_values(agg)[ordo].cumsum() if agg != "month_str" else gmain[gmain.year == int(
                y_ref)].sort_values("month")[ordo].cumsum(),
            marker=dict(color=color,
                        colorbar=colorbar,
                        colorscale=colorscale
                        ),
            name=str(y_ref)
        )
    ]
    for i, y in enumerate(yr):
        data.append(
            dict(
                type="scatter" if data_type is None else data_type,
                mode="markers" if mode is None else mode,
                x=gmain[gmain.year == y].sort_values(agg)[agg] if agg != "month_str" else gmain[gmain.year == y].sort_values("month")[
                    agg],
                y=gmain[gmain.year == y].sort_values(agg)[ordo].cumsum() if agg != "month_str" else gmain[gmain.year == y].sort_values("month")[
                    ordo].cumsum(),
                marker=dict(color=colorpaster[i+1]),
                name=str(y),
                opacity=1
            )
        )
    layout = dict(
        autosize=True,
        automargin=True,
        margin=dict(l=30, r=30, b=20, t=40),
        hovermode="x",
        hovertemplate=None,
        plot_bgcolor="#F9F9F9",
        paper_bgcolor="#F9F9F9",
        title=f"Evolution du chiffre d'affaire cumulé {ordo}",
        colorscale='Inferno'
    )
    return dict(data=data, layout=layout)

############################
# scraping
############################

# get date


def extracter_scraping(con):
    today = str(datetime.now()).split(" ")[0]
    df = pd.read_sql(f"select * from scraping where time >= '{today}'", con)
    df.head()
    df["day"] = df.time.dt.strftime('%d')
    df["year"] = df.time.dt.strftime('%Y')
    df["month"] = df.time.dt.strftime('%m')

    return df

# récupération des chambres dispo sur la période


def get_room(df, hotel, site='booking'):
    today = datetime.now()  # + timedelta(days=n)

    hotel = df[df.measurement == hotel]
    hotel = hotel[hotel.site == site]
    hotel.time = pd.to_datetime(hotel.time, unit='ms')
    
    d = pd.Series(today+timedelta(days=6))[0]
    t = pd.Series(today+timedelta(days=0))[0]

    hotel = hotel.loc[(hotel.time < d) & (hotel.time > t)]

    return hotel.bedType.unique().tolist()


def get_hotel_price(df, hotel, min_value=True, room='global', site='booking'):
    print(
        f'''
        =====
        hotel :     {hotel}
        site :      {site}
        min_value:  {min_value}
        room:       {room}
        =====
        '''
    )
    today = datetime.now()  # + timedelta(days=n)
    df = df[df.measurement == hotel]
    df = df[df.site == site]

    # hotel_booking.time = pd.to_datetime(hotel_booking.time, unit='ms')
    prix = []
    for i in range(6):
        d = pd.Series(today+timedelta(days=i))[0]
        pr = df[df.day == d.day]
        pr = pr[pr.month == d.month]
        pr = pr[pr.year == d.year]
        if room == 'global':
            if min_value:
                prix.append(
                    min(pr.prix)
                )
            else:
                prix.append(
                    max(pr.prix)
                )
        else:
            p = pr.prix[pr.bedType == str(room)]
            if len(p) > 1:
                if min_value:
                    prix.append(
                        min(p)
                    )
                else:
                    prix.append(
                        max(p)
                    )
            elif len(p) == 0:
                prix.append(0)
            else:
                prix.append(p[p.index[0]])
    return prix


def get_date():
    today = datetime.now()
    date = []
    for i in range(6):
        d = today + timedelta(days=i)
        date.append(f'{str(d.day)}.{str(d.month)}')
    return date
