import dash_bootstrap_components as dbc

### Fonction qui créé la barre de navigation
def Navbar(page, lien):

    return dbc.NavbarSimple(
        children=[
            # Affichage du nom de la page active
            dbc.NavItem(
                dbc.NavLink(page, href=lien, className="main_menu"),
                style={'display': 'none'},
            ),
            # Menu déroulant permettant de séléctionner la page
            dbc.NavItem(dbc.NavLink("Home", href="/hub")),
            dbc.NavItem(dbc.NavLink("Suivi", href="/CA")),
            dbc.NavItem(dbc.NavLink("Concurents", href="/concurrence")),
        ],
        brand=page,
        sticky="top",
        # brand_style={"color": "white"},
        color="primary",
        dark=True,
    )