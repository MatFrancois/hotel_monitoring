import dash_bootstrap_components as dbc, dash_auth
from dash import Dash

# VALID_USERNAME_PASSWORD_PAIRS = {
#     'hello': 'world'
# }
### Création de l'objet de type dash
app = Dash(__name__,
            external_stylesheets=[dbc.themes.SANDSTONE]
            )
app.config.suppress_callback_exceptions = True
app.title = 'Tableau de bord Ceitya'
# auth = dash_auth.BasicAuth(
#     app,
#     VALID_USERNAME_PASSWORD_PAIRS
# )
