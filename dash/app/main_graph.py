import dash_bootstrap_components as dbc
# from dash import dcc, html
import dash_html_components as html
import dash_core_components as dcc

from navbar import Navbar

nav = Navbar('Suivi', '/CA')

load_data = html.Button(  # button
    id='main_submit',
    children='Charger la base',
    className="btn btn-primary btn-sm space"
)

change_order = html.Button(  # button
    id='month_order',
    children='Bilan depuis Novembre',
    className="btn btn-primary btn-sm space"
)

upload_data = dcc.Upload(
    id='upload-data',
    children=html.Div(
        'Drag & Drop or Select Files',
    ),
    multiple=True,
    className="space text-position-upload",
)

def change_start_month(id_name='daq_month'):
    return html.Div(
        dbc.Checklist(
            id=id_name,
            options=[{"label": "Mois de départ: Juin", "value": 6}],
            value=[11],
            switch=True,
            className="switcher"
        ), className='space'
    )


selector = dbc.Checklist(id="time_selector")
y_ref = dbc.RadioItems(id="year_ref")

graph_zone = dcc.Graph(id="count_graph")
graph_mois = dcc.Graph(id="count_graph_mois")
graph_sem = dcc.Graph(id="count_graph_sem")

pop_button = dbc.Button("?", id="popover-target", className="btn-secondary space")
pop = dbc.Popover(
    [
        dbc.PopoverHeader("Informations"),
        dbc.PopoverBody(
            "1. Charger la base \n 2. Ajouter des données si besoin \n 3. Choisir une année de comparaison et une de référence")
    ],
    id="popover",
    is_open=False,
    target="popover-target"
)

#  graph pie
pie = dcc.Graph(id="pie_graph")

alerte = dbc.Toast(  # alerte en cas de mauvais format de données / echec
    "Erreur lors de l'importation",
    id="toast_error",
    header="Error",
    is_open=False,
    dismissable=True,
    icon="danger",
    # top: 66 positions the toast below the navbar
    style={"position": "fixed", "top": 66, "right": 10, "width": 350},
)

body_graph = html.Div([
    html.Div([
        # HEADER
        html.Div([
            html.Div([
                html.Div([
                    pop_button,
                    pop,
                    load_data
                ]),
                html.Hr(),
            ], className="col-3"),
            html.Div([
                upload_data,
            ], className='col-9')
            
        ], className="row space"),
        # TABLE
        html.Div([
            alerte,
            html.Div(id='table_zone', className = "col-10"),
            html.Div(id='table_tot', className="col-2")
        ], className="row space"),
        
        html.Div([
            dbc.FormGroup(  # form group
                [
                    html.Hr(),
                    html.H5("Configuration", className="space"),
                    change_start_month(),
                    html.Div(
                        [
                            html.Div([html.Label("Référence"),
                                        y_ref], className="col-6 yref"),
                            html.Div(
                                [html.Label("Comparaison"), selector], className="col-6")
                        ], className="row space"
                    ),
                    dcc.Dropdown(id="y_drop", className="space"),
                    html.Hr()
                ], className="col-lg-3 col-xl-3 col-xs-12 col-sm-12 space"
            ),
            html.Div([  # premier graphique + tableau
                dbc.Tabs(
                    [
                        dbc.Tab(graph_zone, label="Global",
                                className="col-sm-12"),
                        dbc.Tab(graph_mois, label="Mois"),
                        dbc.Tab(graph_sem, label="Semaine")
                    ]
                )], className="col-lg-9 col-xl-9 col-xs-12 col-sm-12 space")
        ], className="row"),
    ], className="container"),
    html.Div(className="container", id="slider_zone"),
    html.Div(
        html.Div(
            html.Div(pie, className="col-12 space"),
            className="row"
        ),
    className="container space"),
    html.Div(
        className="container space",
        id="evo_zone"
    ),
    html.Div(
        className='container space',
        id='cumsum_zone'
    ),

], className="container-fluid")


def Main_Graph():
    return html.Div(
        children=[
            nav,
            body_graph
        ]
    )
