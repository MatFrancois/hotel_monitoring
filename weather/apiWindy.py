import base64
import json
from datetime import datetime

import pandas as pd
import requests
import sqlalchemy as sql

QUERY = 'point/' \
        'ecmwf/' \
        'v2.7/' \
        '46.692/' \
        '-1.947?' \
        'source=detail' \
        '&step=3' \
        '&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MTkyMDI1NTksImluZiI6eyJpcCI6IjkxLjE3NC41Ni44IiwidWEiOiJNb3ppbGxhXC81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdFwvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lXC84OS4wLjQzODkuODYgU2FmYXJpXC81MzcuMzYifSwiZXhwIjoxNjE5Mzc1MzU5fQ.k3uhhOZzpNHff_ETY6QiyLUfuk8UO_TX7ukcQ-BeLhg' \
        '&token2=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtYWdpYyI6MTEyLCJpYXQiOjE2MTkyMDI1NjAsImV4cCI6MTYxOTM3NTM2MH0.Aq4lraYBJMUyGiUuS1D_zlPwUOcNkVShbBl84YT2Lpk' \
        '&uid=9da02b54-f263-da27-187c-b0adef4df39f' \
        '&sc=11' \
        '&pr=0' \
        '&v=29.2.4' \
        '&poc=19'
URL = 'https://node.windy.com/Zm9yZWNhc3Q/ZWNtd2Y/' + str(base64.b64encode(QUERY.encode("utf-8")), "utf-8")

def dbConnexion(url="postgresql://postgres:mypassword@postgres:5432/postgres"):
    engine = sql.create_engine(url)
    return engine.connect()
    

def requestWindy(url):
    """request windy and return a pandas dataframe

    Args:
        url (str): url to request with base64 encoded query

    Raises:
        ValueError: _description_

    Returns:
        pd.DataFrame: pandas dataframe with windy data
    """
    
    r = requests.get(url)

    if r.status_code != 200:
        raise ValueError("status != 200")

    # decode du base64
    response = r.text
    forecast = json.loads(str(base64.b64decode(response), "utf-8"))
    print(forecast)
    # Conversion en table
    df = pd.DataFrame()
    df['date'] = forecast['data']['origDate']
    df['temp'] = forecast['data']['temp']
    df['temp'] = df['temp'].apply(lambda x: x-274.15)
    df['icon'] = forecast['data']['icon']
    df['wind_kts'] = forecast['data']['wind']
    df['rain_mm'] = forecast['data']['mm']
    df['dat_exec'] = pd.Series(datetime.now())[0]
    
    return df

def main():
    con = dbConnexion()
    try:
        query = f"DELETE FROM weather WHERE date > '{str(pd.Series(datetime.now())[0])}';"
        con.execute(query)
    except Exception:
        print("no data")
    
    df = requestWindy(URL)
    print(df)
    try:
        frame = df.to_sql("weather", con, if_exists='append')
    except ValueError as vx:
        print(vx)
    
# save dans notre db
# df.to_csv('~/Documents/Job/Projet/eric/dash/weather/data.csv',
#           mode='a', header=False)

if __name__ == "__main__":
    main()
