import time, requests, json

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from fake_useragent import UserAgent
from bs4 import BeautifulSoup

class MediumScraper:

    def __init__(self):
        self.options = Options()
        # self.ua = UserAgent()
        # self.userAgent = self.ua.random
        # print(self.userAgent)
        # self.options.add_argument(f'user-agent={self.userAgent}')

        self.driver = webdriver.Firefox(executable_path='./geckodriver')

    def get(self, url=''):
        self.driver.get(url)

    def extract(self, xpath):
        return [t.text for t in self.driver.find_elements_by_xpath(xpath)]

    def bs(self):
        return BeautifulSoup(self.driver.page_source, 'html.parser')

    def bsBookingExtract(self, bso=None):
        """extract from a bs object

        :param regex: regex
        :param name: class or id
        :param tag: tag to extract
        :param bso: bs object
        :return:
        """
        if bso == None:
            bso = self.bs()
        tt = bso.find_all('table', {'class': 'hprt-table hprt-table-long-language'})
        
        if len(tt)>0:
            print('oui')
            firsttr = tt[0].find('tbody')
            res = firsttr.find_all('tr')
            outNom = []
            outPrix = []
            nom = None
            for t in res:
                prix = t.find('span', {'class': "prco-valign-middle-helper"})
                if (t.find('span', {'class': "hprt-roomtype-icon-link"}) is not None) and (prix is not None):
                    nom = t.find('span', {'class': "hprt-roomtype-icon-link"})
                if prix is not None:
                    outNom.append(nom.text.strip())
                    outPrix.append(prix.text.strip().replace('€\xa0',''))
            print(outPrix)
            return outNom, outPrix


def jsSterneExtract(url):
    r = requests.get(url)
    if r.status_code != 200:
        return
    j = json.loads(r.text.strip(')('))
    time.sleep(1)
    try:
        long = len(j['Reponse']['items'][0]['metier']['items'])
        res = j['Reponse']['items'][0]['metier']['items']
        prix = []
        nom = []
        for l in range(long):
            prix.append(res[l]['Prix'])
            nom.append(res[l]['libelle'].lower())
            return nom, prix
    except IndexError:
        return

