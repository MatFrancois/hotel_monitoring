# BOOKING CRAWLER

Script de scrapping des prix proposés par différents hotels via site perso et booking malgrés js.

Historisation : Quotidienne

Localisation : db_cont.scraping

## Format

```
 index | measurement |  site   |                  bedType                  |        time         | prix
-------+-------------+---------+-------------------------------------------+---------------------+------
     0 | ******      | booking | full                                      | 2020-10-30 00:00:00 |    0
     1 | *******     | booking | full                                      | 2020-10-30 00:00:00 |    0
     2 | ****        | booking | Chambre Standard avec 1 Lit Simple        | 2020-10-30 00:00:00 |   78
     3 | ****        | booking | Chambre Standard avec 2 Lits Simples      | 2020-10-30 00:00:00 |   88
     4 | ****        | booking | Chambre Standard avec 2 Lits Simples      | 2020-10-30 00:00:00 |   98
     5 | ****        | booking | Chambre Standard avec 1 Lit Double        | 2020-10-30 00:00:00 |   92
```

_cron quotidien_
