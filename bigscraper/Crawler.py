import random as r
import time
from datetime import datetime, timedelta

import pandas as pd
import sqlalchemy as sql
from bs4 import BeautifulSoup

from Base import MediumScraper, jsSterneExtract


# connexion à la base
def dbConnexion(url="postgresql://postgres:mypassword@postgres:5432/postgres"):
    print('======CONNEXION======')
    engine = sql.create_engine(url)
    return engine.connect()

def bookingExtract(urls, hotel, MyCrawler, dateIn, data):
    print('======BOOKING======')
    for url, name in zip(urls, hotel):
        print(f'working on {name}')
        MyCrawler.get(url)
        try: 
            time.sleep(r.randint(3,5))
        except TypeError:
            time.sleep(4)
            print("type error")
        ret = MyCrawler.bsBookingExtract()
        if ret is not None:
            nom, prix = ret
            if (len(prix) > 0):
                print('nb prix : ',name,' - ',len(nom))
                for nm, p in zip(nom, prix):
                    data.append({
                        'measurement': name,
                        'site': 'booking',
                        'bedType': nm,
                        'time': pd.Series(dateIn).astype('datetime64')[0],
                        'prix': int(p)
                    })
            else:
                print('write full')
                data.append({
                    'measurement': name,
                    'site': 'booking',
                    'bedType': 'full',
                    'time': pd.Series(dateIn).astype('datetime64')[0],
                    'prix': 0
                })
        else:
            print('write full')
            data.append({
                'measurement': name,
                'site': 'booking',
                'bedType': 'full',
                'time': pd.Series(dateIn).astype('datetime64')[0],
                'prix': 0
            })
                    
def ibisExtract(MyCrawler, data, dateIn, url):
    print('======IBIS PERSO======')
    MyCrawler.get(url)
    try:
        time.sleep(r.randint(8,10))#time.sleep(5)
    except TypeError:
        time.sleep(10)
        print("type error a")
    sc = BeautifulSoup(MyCrawler.driver.page_source, 'html.parser')
    chambres = sc.find_all(class_='room-card__title')
    if (chambres == []):
        data.append({
            'measurement': 'ibis',
            'site': 'perso',
            'bedType': 'full',
            'time': pd.Series(dateIn).astype('datetime64')[0],
            'prix': 0
        })
    else:
        print('chambre: ok')
        try:
            prix = [nr.find(class_='booking-price__number').text for nr in sc.find_all(class_='room')]
            for chambre, tarif in zip(chambres, prix):
                data.append({
                    'measurement': 'ibis',
                    'site': 'perso',
                    'bedType': chambre.text,
                    'time': pd.Series(dateIn).astype('datetime64')[0],
                    'prix': int(tarif)
                })
        except Exception as e:
            data.append({
                'measurement': 'ibis',
                'site': 'perso',
                'bedType': 'full',
                'time': pd.Series(dateIn).astype('datetime64')[0],
                'prix': 0
            })
            print('full')
            print(e)

def ceityaExtract(MyCrawler, data, dateNjour, url):
    print('======CEITYA PERSO======')
    # ceitya perso part
    MyCrawler.get(url)
    try:
        time.sleep(r.randint(5,7))
    except TypeError:
        time.sleep(6)
        print("type error b")
    sc = BeautifulSoup(MyCrawler.driver.page_source, 'html.parser')

    # room zone
    nroom = sc.find_all(id='booking-room-component')

    if len(nroom) > 0:
        for nr in nroom:
            for i in nr.find_all(class_='name'):
                chambres = i.find(class_="ng-star-inserted").text
            prix = nr.find(class_="price-info-price ng-star-inserted").text
            prix = prix.replace('\xa0€','').split(',')[0]
            data.append({
                'measurement': 'ceitya',
                'site': 'perso',
                'bedType': chambres.lower(),
                'time': pd.Series(f'{dateNjour.year}-{dateNjour.month}-{dateNjour.day}').astype('datetime64')[0],
                'prix': int(prix)
            })
    else:
        data.append({
            'measurement': 'ceitya',
            'site': 'perso',
            'bedType': 'full',
            'time': pd.Series(f'{dateNjour.year}-{dateNjour.month}-{dateNjour.day}').astype('datetime64')[0],
            'prix': 0
        })

def seleniumExtract(n=0, hotel=None):
    """extract data with selenium
    
    Args:
        n (int, optional): how many days to start from today. Defaults to 0.
        hotel (list, optional): hotel list to request. Defaults to None.

    Returns:
        list: data list
    """
    
    if hotel is None:
        hotel = ['sterne','seaView','ibis','ceitya']
    data = []
    print('======WHILE LOOP======')

    while n<8:
        MyCrawler = MediumScraper()
        print(f"running... n = {n}")

        dateNjour = datetime.now() + timedelta(days=n)
        dateIn = f'{dateNjour.year}-{dateNjour.month}-{dateNjour.day}'
        dateN1jour = dateNjour + timedelta(days=1)
        dateOut = f'{dateN1jour.year}-{dateN1jour.month}-{dateN1jour.day}'

        print(f'----RUNNING DATE: {dateIn}----')

        urls = [
            f'https://www.booking.com/hotel/fr/la-sterne.fr.html?checkin={dateIn};checkout={dateOut};group_adults=1;group_children=0;no_rooms=1;req_adults=1;req_children=0;',
            f'https://www.booking.com/hotel/fr/le-ker-louis.fr.html?checkin={dateIn};checkout={dateOut};group_adults=1;group_children=0;no_rooms=1;req_adults=1;req_children=0;',
            f'https://www.booking.com/hotel/fr/aa1106853803873.fr.html?checkin={dateIn};checkout={dateOut};group_adults=1;group_children=0;no_rooms=1;req_adults=1;req_children=0;',
            f'https://www.booking.com/hotel/fr/le-ceitya.fr.html?checkin={dateIn};checkout={dateOut};group_adults=1;group_children=0;no_rooms=1;req_adults=1;req_children=0;',
        ]

        bookingExtract(urls=urls, hotel=hotel, MyCrawler=MyCrawler, data=data, dateIn=dateIn)
    # print('----PRICE FOUND: {}----'.format(len(prix)))

        ibis = f"https://all.accor.com/ssr/app/accor/rates/8951/index.fr.shtml?dateIn={dateIn}&nights=1&compositions=1&stayplus=false&destination=Saint%20gilles%20croix%20de%20vie,%20France"
        ibisExtract(MyCrawler=MyCrawler, data=data, dateIn=dateIn, url=ibis)

        dateIn = f'{dateNjour.day}-{dateNjour.month}-{dateNjour.year}'
        dateOut = f'{dateN1jour.day}-{dateN1jour.month}-{dateN1jour.year}'
        url = f'https://secure.reservit.com/fo/booking/2/170968/availability?nbroom=1&specialMode=default&begindate={dateIn}&tmonth=07&tyear=2020&hotelid=170968&numnight=1&m=booking&roomAge1=40,40&langcode=FR&enddate={dateOut}'
        ceityaExtract(MyCrawler=MyCrawler, data=data, dateNjour=dateNjour, url=url)
    # print('----PRICE FOUND: {}----'.format(len(prix)))
    # print('----PRICE FOUND: {}----'.format(len(nroom)))
        n +=1
        MyCrawler.driver.quit()
    return data

def sterneExtract(data):
    """extract data from personal sterne site

    Args:
        data (list): list data to be filled
    """
    print('======STERNE PERSO======')
    for n in range(8):
        print(n)
        dateNjour = datetime.now() + timedelta(days=n)
        m = str(dateNjour.month)
        m = m if len(m) == 2 else f'0{m}'
        d = str(dateNjour.day)
        d = d if len(d) == 2 else f'0{d}'
        dateIn = f'{dateNjour.year}-{m}-{d}'

        url = f'https://etape-rest.for-system.com/index.aspx?ref=json-catalogue-etape22&q=fr,HRIT-30431,vendee,{dateIn},1,1,,*,*,0'
        ret = jsSterneExtract(url)
        if ret is not None:
            nom, prix = ret
            for nm, p in zip(nom, prix):
                print(nm, ' ', p)
                data.append({
                    'measurement': 'sterne',
                    'site': 'perso',
                    'bedType': nm,
                    'time': pd.Series(dateIn).astype('datetime64')[0],
                    'prix': int(p)
                })
        else:
            data.append({
                'measurement': 'sterne',
                'site': 'perso',
                'bedType': 'full',
                'time': pd.Series(dateIn).astype('datetime64')[0],
                'prix': 0
            })
    return data
    # print('----PRICE FOUND: {}----'.format(len(prix)))

def dropData(con):
    """suppress value older than 

    Args:
        con (_type_): postgres sqlalchemy connexion
    """
    print('======DROP OLD DATA======')
    try:
        query = f"DELETE FROM scraping WHERE time >= '{pd.Series(datetime.now())[0]}';"
        con.execute(query)
    except Exception:
        print('no delete')

def pushData(df, con):
    print('======PUSH NEW DATA======')
    try:
        print(df.columns)
        print(df)
        df.to_sql("scraping", con, if_exists='append')
    except ValueError as vx:
        print(vx)

def main():
    co = dbConnexion()

    data = seleniumExtract()
    df = pd.DataFrame(sterneExtract(data))
    print(data)
    dropData(con=co)
    
    pushData(df=df, con=co)
    
    
    
if __name__ == "__main__":
    time.sleep(7)
    main()
